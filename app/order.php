<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class order extends Model
{
    protected $fillable = ['firstname' , 'lastname' , 'state' , 'city' , 'address' , "zipcode" , "phone" , "email" , "desc" , "status" , "message" , "product" , "price", "id"];
}
