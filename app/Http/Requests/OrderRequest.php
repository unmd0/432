<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'required|max:50',
            'lastname' => 'required|max:50',
            'state' => 'required|max:50',
            'city' => 'required|max:50',
            'address' => 'required|max:120',
            'zipcode' => 'required|max:50',
            'phone' => 'required|max:50',
            'email' => 'email:rfc,dns|max:70|nullable',
            'desc' => 'max:255',
        ];
    }
}
