<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Http\Requests\ProfileRequest;
use App\order;
use App\orderproduct;
use App\product;
use App\profile;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Morilog\Jalali\Jalalian;

class ProductController extends Controller
{
    public function home()
    {
        $articles = product::orderBy('id' , 'desc')->get();
        return view('index');
    }

    //-----------------------------\\
    //-----------------------------\\
    //-----product Controller------\\
    //-----------------------------\\
    //-----------------------------\\
    //|_______|\\
    //|STORE|\\
    //|SORE PRODUCT|\\
    public function create(ProductRequest $request)
    {
        $validate_data = $request->validated();

        product::create([
            'name' => $validate_data['name'],
            'pic' => $validate_data['pic'],
            'category' => $validate_data['category'],
            'keywords' => $validate_data['keywords'],
            'desc' => $validate_data['desc']
        ]);
        return redirect('/admin/type/create/' . DB::getPdo()->LastInsertId());
    }
    //|_______|\\
    //|REDIRECT|\\
    //|SHOW PRODUCT|\\
    public function showproduct()
    {
        $alltypes=product::paginate(10);
        return view('admin.product.show',["types"=>$alltypes]);
    }
    //|_______|\\
    //|SEARCH---$_GET|\\
    //|SEARCH ON PRODUCTS LIST|\\
    public function searchproduct()
    {
        $search1 =$_GET["search1"];
        $alltypes=product::where("name","LIKE","%".$search1."%")->paginate(10);
        
    }
    //|_______|\\
    //|REDIRECT|\\
    //|PAGE OF EDIT PRODUCT|\\
    public function editproduct($ID)
    {
        $alltypes=product::where('productID',"=",$ID)->firstOrFail();
        $category = DB::table('category')->get();
        return view('admin.product.edit',['types'=>$alltypes,'category'=>$category]);
    }
    //|_______|\\
    //|UPDATE|\\
    //|UPDATE PRODUCTS|\\
    public function updateproduct(ProductRequest $request)
    {
        $validate_data = $request->validated();
        product::where('productID',"=",$validate_data['productID'])->update([
            'name' => $validate_data['name'],
            'pic' => $validate_data['pic'],
            'category' => $validate_data['category'],
            'keywords' => $validate_data['keywords'],
            'desc' => $validate_data['desc']
        ]);
        return redirect('/admin/product/show');
    }
    //|_______|\\
    //|_______|\\
    //|_______|\\
    //-------END product Controller-------\\












    //-----------------------------\\
    //-----------------------------\\
    //-------Type Controller-------\\
    //-----------------------------\\
    //-----------------------------\\
    //|_______|\\
    //|REDIRECT|\\
    //|CREATE & EDIT TYPES|\\
    public function createtype($id)
    {
        $types=DB::table('big_s_products_type')->where('ProductId',"=",$id)->get();
        return view('admin.types.add',["ID"=>$id,"typeAll"=>$types]);
    }
    //|_______|\\
    //|REDIRECT|\\
    //|LIST TYPES|\\
    public function showtype()
    {
        $alltypes=DB::table('big_s_products_type')->join('products','products.productID','=','big_s_products_type.ProductId')->paginate(10);
        return view('admin.types.show',["types"=>$alltypes]);
    }
    //|_______|\\
    //|SEARCH---$_GET|\\
    //|SEARCH TYPE|\\
    public function searchtype()
    {
        $search1 =$_GET["search1"];
        $alltypes=DB::table('big_s_products_type')->join('products','products.productID','=','big_s_products_type.ProductId')->where("big_s_products_type.Name","LIKE","%" . $search1 . "%")->orWhere("products.name","LIKE","%".$search1."%")->paginate(10);
        return view('admin.types.show',["types"=>$alltypes,"q"=>$search1]);
    }
    //|_______|\\
    //|_______|\\
    //|_______|\\
    //-----------------------------\\
    //-----------------------------\\
    //-----END Type Controller-----\\
    //-----------------------------\\
    //-----------------------------\\










    //-----------------------------\\
    //-----------------------------\\
    //-----Category Controller-----\\
    //-----------------------------\\
    //-----------------------------\\
    //|_______|\\
    //|REDIRECT|\\
    //|CATEGORY SHOW & EDIT & UPDATE|\\
    public function category($id)
    {
        $category = DB::table('category')->where('pid',"=",$id)->get();
        $menuname = DB::table('category')->where('id',"=",$id)->get();
        return view('admin.category.index',['categorys'=>$category,'id'=>$id,'cname1'=>$menuname]);
    }
    //-----------------------------\\
    //-----------------------------\\
    //---END Category Controller---\\
    //-----------------------------\\
    //-----------------------------\\







    //-----------------------------\\
    //-----------------------------\\
    //------Orders Controller------\\
    //-----------------------------\\
    //-----------------------------\\
    //|_______|\\
    //|REDIRECT|\\
    //|Orders SHOW|\\
    public function orders()
    {
        $alltypes=order::orderBy('created_at', 'DESC')->paginate(10);
        return view('admin.orders.show',["orders"=>$alltypes]);
    }
    //|_______|\\
    //|REDIRECT|\\
    //|Orders Details SHOW|\\
    public function ordersdetails($id)
    {
         if (order::where("id","=",$id)->first()->shopstatus == "0"){
            order::where("id","=",$id)->update([
                'shopstatus' => '1'
                ]);
         }

        $alltypes=orderproduct::where("orderid","=",$id)->join('orders','orders.id','=','orderproducts.orderid')->paginate(10);
        // $alltypes=DB::table('big_s_products_type')->join('products','products.productID','=','big_s_products_type.ProductId')->paginate(10);
        return view('admin.orders.details',["orders"=>$alltypes]);
    }
    //|_______|\\
    //|REDIRECT|\\
    //|Orders Details SHOW|\\
    public function ordership($id)
    {
        order::where("id","=",$id)->update([
            'shopstatus' => '2'
            ]);
        return redirect()->back()->with('message', 'تغییر وضعیت سفارش به آماده ارسال');
    }
    //-----------------------------\\
    //-----------------------------\\
    //---END Orders Controller---\\
    //-----------------------------\\
    //-----------------------------\\
    


    //-----------------------------\\
    //-----------------------------\\
    //------Profile Controller------\\
    //-----------------------------\\
    //-----------------------------\\
    //|_______|\\
    //|REDIRECT|\\
    //|Orders SHOW|\\
    public function profile()
    {
        
        $profile=profile::where('userid',"=",Auth::user()->name)->first();
        return view('admin.profile.add',['profile'=>$profile]);
    }
    //|_______|\\
    //|REDIRECT|\\
    //|Orders Details SHOW|\\
    public function profileadd($id)
    {
        $alltypes=orderproduct::where("orderid","=",$id)->join('orders','orders.id','=','orderproducts.orderid')->paginate(10);
        // $alltypes=DB::table('big_s_products_type')->join('products','products.productID','=','big_s_products_type.ProductId')->paginate(10);
        return view('admin.orders.details',["orders"=>$alltypes]);
    }
    //|_______|\\
    //|REDIRECT|\\
    //|Orders Details SHOW|\\
    public function updateprofile(ProfileRequest $request)
    {
        $validate_data = $request->validated();
        // 
        profile::where('userid',"=",Auth::user()->name)->update([
            'name' => $validate_data['name'],
            'telegram' => $validate_data['telegram'],
            'instagram' => $validate_data['instagram'],
            'logo' => $validate_data['logo'],
            'address' => $validate_data['address'],
            'phone' => $validate_data['phone'],
            'irshaba' => $validate_data['irshaba'],
            'validatepic' => $validate_data['validatepic']
        ]);
         return redirect()->back()->with('message', 'تغییرات با موفقیت انجام شد');
    }
    //-----------------------------\\
    //-----------------------------\\
    //---END Orders Controller---\\
    //-----------------------------\\
    //-----------------------------\\






}
