<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderRequest;
use App\order;
use App\orderproduct;
use App\product;
use Carbon\Carbon;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StoreController extends Controller
{
    //|_______|\\
    //|_______|\\
    //|_______|\\
    //-------product Controller-------\\
    //|_______|\\
    //|_______|\\
    //|_______|\\
    public function home(){
        $products = product::orderBy('productID' , 'desc')->get();
        return view('shop.index',['products'=>$products]);
    }
    //|_______|\\
    //|REDIRECT|\\
    //|SHOW PRODUCT|\\
    public function details($sitename,$id){
        $products = DB::table('products')->where("productID","=",$id)->first();
         $types = DB::table('big_s_products_type')->where("ProductId","=",$id)->orderBy('Price' , 'asc')->get();
        return view('shop.details',['products'=>$products,'types'=>$types,'id'=>$id]);
    }
    //|_______|\\
    //|_______|\\
    //|_______|\\
    //-------END product Controller-------\\
    
    
    
    


    //-------Cart Controller-------\\
    //|_______|\\
    //|_______|\\
    //|_______|\\
    public function addcart($sitename,$id2,Request $request) {
        $id = $request->input('id');
        $tid = $request->input('tid');
        $name = $request->input('name');
        $pic = $request->input('pic');
        $type = DB::table('big_s_products_type')->where("TypeId","=",$id)->first();
       $qty = $request->input('qty');
        $price = $request->input('price');
        if ($id==$id2){
        Cart::add($id, $name, $qty, $price,0, ["type" => $type, "pic" => $pic]);
        }
        echo(Cart::count());
    }
    //|_______|\\
    //|CART-AJAX|\\
    //|UPDATE CART QUANTITY|\\
    public function updatecart(Request $request) {
        $id = $request->input('id');
       $qty = $request->input('qty');
        Cart::update($id, $qty);
        $back=[Cart::count(),Cart::get($id)->subtotal(0),Cart::subtotal(0)];
        echo(json_encode($back));
    }
    //|_______|\\
    //|CART-AJAX|\\
    //|DELETE PRODUCT FROM CART|\\
    public function deletecart(Request $request) {
        $id = $request->input('id');
        Cart::remove($id);
        $back=[Cart::count(),Cart::subtotal(0)];
        echo(json_encode($back));
    }
    //|_______|\\
    //|REDIRECT|\\
    //|SHOW CART|\\
    public function cart(){
        $carts=Cart::content();
        return view("shop.cart",['carts'=>$carts]);
    }
    //|_______|\\
    //|_______|\\
    //|_______|\\
    //-------END cart Controller-------\\
    
    
    
    
    
    
    

    //-------checkout Controller-------\\
    //|_______|\\
    //|_______|\\
    //|_______|\\
    public function checkout(){
        $carts=Cart::content();
        return view("shop.checkout",['carts'=>$carts]);
    }
    //|_______|\\
    //|_______|\\
    //|_______|\\
    public function checkoutstore(OrderRequest $request){

        $carts=Cart::content();
        $validate_data = $request->validated();
        $idgenerator = Carbon::now()->timestamp . Carbon::now()->milli;
        order::create([
            'firstname' => $validate_data['firstname'],
            'lastname' => $validate_data['lastname'],
            'state' => $validate_data['state'],
            'city' => $validate_data['city'],
            'address' => $validate_data['address'],
            'zipcode' => $validate_data['zipcode'],
            'phone' => $validate_data['phone'],
            'email' => $validate_data['email'],
            'desc' => $validate_data['desc'],
            'status' => "در انتظار پرداخت",
            'id' => $idgenerator,
            'price' => Cart::subtotal1(35000,0),
            'product' => serialize(Cart::content())
        ]);
        foreach($carts as $item)
        {
            $n=$item->name;
            $t=$item->options->type->Name;
            $n= $n . "-" . $t;
            orderproduct::create([
                'orderid' => $idgenerator,
                'typeid' => $item->id,
                'name' =>   $n,
                'pic' => $item->options->pic,
                'priceeach' => number_Format($item->price),
                'qty' => $item->qty,
                'subtotal' => $item->subtotal(0)
            ]);
        }
        
        Cart::destroy();
        return view('shop.checkoutresponse' , ['orderID'=>$idgenerator]);
    }
    //|_______|\\
    //|_______|\\
    //|_______|\\
    public function ordertrack(Request $request){
        
        $order="";
        $q="";
    if($request->q){
            $q=$request->q;
            $id = $request->q;
            $order=orderproduct::where("orderid","=",$id)->join('orders','orders.id','=','orderproducts.orderid')->paginate(10);
    }
    
        return view("shop.ordertracking",['order'=>$order,'q'=>$q]);
    }
    //-------End checkout Controller-------\\






}
