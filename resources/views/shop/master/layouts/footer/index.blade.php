<footer class="footer spad hide" id="footer">
    <div class="container">
        <div class="row">
<div style="display: flex;margin: auto;list-style-type: none;">
    <li><a href="#"><i class="fa fa-telegram" style="color: #0088CC;font-size: 35px;border-radius: 100%;padding: 2px 6px;"></i></a></li>
    <li><a href="#"><i class="fa fa-instagram instafooter"></i></a></li>
</div>
<form action="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/order/track" method="post" style="display: flex">
    @csrf
    <input type="text" name="q" class="hide">
<button class="btn brn-success" type="submit">پیگیری سفارش</button>
</form>
                </div>
            </div>
</footer>
<script src="/js/organi/jquery-3.3.1.min.js"></script>
<script src="/js/organi/bootstrap.min.js"></script>
{{-- <script src="/js/organi/jquery.nice-select.min.js"></script> --}}
<script src="/js/organi/jquery-ui.min.js"></script>
<script src="/js/organi/jquery.slicknav.js"></script>
<script src="/js/organi/mixitup.min.js"></script>
<script src="/js/organi/owl.carousel.min.js"></script>
<script src="/js/organi/main.js"></script>
<script>
        function updatecart(X,Y){
        var myJsonData = { "_token": "{{ csrf_token() }}", id: Y, qty: X }
    $.post('{{url()->current()}}/updatecart', myJsonData, function(response) {
        var responce = JSON.parse(response);
    document.getElementById("cartcount").innerHTML=toPersianNum(responce[0]);
    document.getElementById(Y).innerHTML=toPersianNum(responce[1]);
    document.getElementById("subtotal").innerHTML=toPersianNum(responce[2]);
});
}
function deletecart(X,Y){
    window.Z;
X.parentElement.remove();

    var myJsonData = { "_token": "{{ csrf_token() }}", id: Y }
    $.post('{{url()->current()}}/deletecart', myJsonData, function(response) {
        var responce = JSON.parse(response);
    document.getElementById("cartcount").innerHTML=toPersianNum(responce[0]);
    document.getElementById("subtotal").innerHTML=toPersianNum(responce[1]) ;
    emptycart(responce[0]);
});

}
function emptycart(X){
    // alert(X);
    // var X = document.getElementById("cartcount").innerHTML;
    if (X != 0){
        document.getElementById('emptycart').classList.add('hide');
        document.getElementById('checkoutf').classList.remove('hide');
        document.getElementById('fillcart').classList.remove('hide');
    }
    else{
        document.getElementById('fillcart').classList.add('hide');
        document.getElementById('checkoutf').classList.add('hide');
        document.getElementById('emptycart').classList.remove('hide');
    }

}

$( document ).ready(function() {
    document.getElementById('main').classList.remove('hide');
    document.getElementById('footer').classList.remove('hide');
    
     
});
function toPersianNum( num, dontTrim ) {

var i = 0,

    dontTrim = dontTrim || false,

    num = dontTrim ? num.toString() : num.toString().trim(),
    len = num.length,

    res = '',
    pos,

    persianNumbers = typeof persianNumber == 'undefined' ?
        ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'] :
        persianNumbers;

for (; i < len; i++)
    if (( pos = persianNumbers[num.charAt(i)] ))
        res += pos;
    else
        res += num.charAt(i);

return res;
}
</script>