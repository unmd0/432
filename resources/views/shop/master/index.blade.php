<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>فروشگاه</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
@yield('meta_add')
    @include('shop.master.layouts.links.index')
    
</head>

<body>
    @include('shop.master.layouts.header.index')
    {{-- @include('shop.master.layouts.menu.index') --}}
    @yield('shop-main')
    @include('shop.master.layouts.footer.index')
</body>

</html>