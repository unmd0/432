@extends('shop.master.index')
@section('shop-main')
        <!-- Featured Section Begin -->
        <section class="featured spad hide" id="main">
            <div class="sr">
                <form action="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/order/track" method="post">
                    @csrf
                <input type="search" name="q" value="{{$order}}">
                    <i class="fa fa-search"></i>
                </form>
            </div>
            {{-- {{dd($order)}} --}}
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="shoping__cart__table">
                            @if($q=="")
                            <div class="checkout__title">
                                <h2>شماره سفارش خود را در کادر بالا جستجو کنید</h2>
                            </div>
            @elseif ($order <> "")

                    <table id="fillcart">
                        <thead>
                            <tr>
                                <th class="shoping__product">محصول</th>
                                <th>قیمت</th>
                                <th>تعداد</th>
                                <th>قیمت کل</th>
                                <th>وضعیت</th>
                            </tr>
                        </thead>
                    <tbody>
                        <?php $i=1 ?>
            
                        @foreach($order as $item)
                            <tr>
                                <tr>
                                    <td class="shoping__cart__item">
                                        <img src="{{$item->pic}}" alt=""><br class="br" style="display: none;"><br class="br" style="display: none;">
                                        <h5> {{$item->name}}</h5>
                                    </td>
                                    <td class="shoping__cart__price" style="text-align: bottom;">
                                        {{MPCO\EnglishPersianNumber\Numbers::toPersianNumbers(str_replace(',', '', $item->priceeach),3)}} تومان
                                    </td>
                                <td>{{ $item->qty }}</td>
                                <td class="shoping__cart__total" id="{{$item->rowId}}">
                                    {{MPCO\EnglishPersianNumber\Numbers::toPersianNumbers(str_replace(',', '', $item->subtotal),3)}} تومان
                                </td>
                                <td>
                                    {{-- <a href="/admin/product/edit/{{$item->productID}}"> --}}
                                        @switch($item->shopstatus)
                                            @case(0)
                                            مشاهده نشده
                                                @break
                                            @case(1)
                                            درحال آماده سازی
                                                @break
                                            @case(2)
                                            آماده ارسال  
                                                @break    
                                            @default
                                                
                                        @endswitch
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
<div class="checkout__title">
    <h2 style="background-color: #ffe1e1 !important;"><i class="fa fa-times" style="padding-left: 10px;color:red;"></i>شماره سفارش وارد شده صحیح نمیباشد</h2>
</div>
            @endif
                        </div></div></div></div>
        </section>
@endsection