@extends('shop.master.index')
@section('shop-main')
<section class="checkout spad">
    <div class="container">
        <div class="checkout__form">
            <h4>اطلاعات ارسال</h4>
            @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
            <form action="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/checkoutstore" method="POST">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-lg-8 col-md-6">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="checkout__input">

                                    <p>نام<span>*</span></p>
                                    <input type="text" name="firstname">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="checkout__input">
                                    <p>نام خانوادگی<span>*</span></p>
                                    <input type="text" name="lastname">
                                </div>
                            </div>
                        </div>
                        <div class="checkout__input">
                            <p>استان<span>*</span></p>
                            <input type="text" name="state">
                        </div>
                        <div class="checkout__input">
                            <p>شهر<span>*</span></p>
                            <input type="text"  name="city">
                        </div>
                        <div class="checkout__input">
                            <p>آدرس<span>*</span></p>
                            <input type="text"  name="address" placeholder="آدرس پستی" class="checkout__input__add">
                        </div>

                        <div class="checkout__input">
                            <p>کد پستی<span>*</span></p>
                            <input type="text"  name="zipcode">
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="checkout__input">
                                    <p>شماه همراه<span>*</span></p>
                                    <input type="text"  name="phone">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="checkout__input">
                                    <p>ایمیل</p>
                                    <input type="text"  name="email">
                                </div>
                            </div>
                        </div>
                        
                        <div class="checkout__input">
                            <p>توضیحات</p>
                            <input type="text"
                                placeholder="توضیحات سفارش" name="desc">
                        </div>
                        <div class="checkout__input__checkbox">
                            <label for="acc">
                                ذخیره اطلاعات ارسال برای سفارشات بعدی شما
                                <input type="checkbox" id="acc">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <p>با فعال کردن
                             این گزینه اطلاعات ارسال ورودی در مرورگر 
                            شما ذخیره میشود و در خریدهای بعدی
                             میتوانید بدون وارد کردن
                             اطلاعات ارسال سفرش خود را نهایی کنید</p>

                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="checkout__order">
                            <h4>سفارشات شما</h4>
                            <div class="checkout__order__products">سفارشات</div>
                            <ul>
                                @foreach ($carts as $item)
                                <li>{{$item->name}}<span>تومان</span> <span class="price">{{MPCO\EnglishPersianNumber\Numbers::toPersianNumbers($item->subtotal,3)}}</span></li>
                                @endforeach
                            </ul>
                            <div class="checkout__order__subtotal">جمع خرید <span>تومان</span><span class="price">{{MPCO\EnglishPersianNumber\Numbers::toPersianNumbers(Cart::subtotal(0,0,""),3)}}</span></div>
                            <div class="checkout__order__total">هزینه ارسال <span>تومان</span><span class="price">{{MPCO\EnglishPersianNumber\Numbers::toPersianNumbers("35000",3)}}</span></div>
                            <div class="checkout__order__total">پرداختی <span>تومان</span><span class="price">{{MPCO\EnglishPersianNumber\Numbers::toPersianNumbers(Cart::subtotal1(35000,0,0,""),3)}}</span></div>
                            <button type="submit" class="site-btn">پرداخت</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection