@extends('shop.master.index')
@section('shop-main')
<section class="featured spad hide" id="main">
    <div class="col-lg-12">
        <div class="checkout__title">
            <h2><i class="fa fa-check-circle" style="padding-left: 10px;color:#54a6ff;"></i>سفارش شما با موفقیت انجام شد</h2>
        </div>
        <div class="featured__controls">
            <ul>
                <li class="active">لطفا شماره سفارش را برای پیگیری یادداشت نمایید</li><br/>
            <li class="active" style="padding-top: 25px">شماره سفارش: {{MPCO\EnglishPersianNumber\Numbers::toPersianNumbers($orderID)}}</li>
            </ul>
        </div>
    </div>
    </section>

@endsection