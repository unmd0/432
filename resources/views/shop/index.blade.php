@extends('shop.master.index')
@section('shop-main')
        <!-- Featured Section Begin -->
        <section class="featured spad hide" id="main">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-title">
                            <h2>فال گالری</h2>
                        </div>
                        <div class="featured__controls">
                            <ul>
                                <li class="active" data-filter="*">همه</li>
                                @foreach (DB::table('category')->get() as $item)
                                    <li data-filter=".{{$item->cname}}">{{$item->cname}}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row featured__filter">

                    @foreach ($products as $product)
                    
                    <div class="col-lg-3 col-md-4 col-sm-6 mix oranges {{$product->category}}">
                        <a href="{{url()->current()}}/details/{{$product->productID}}">
                        <div class="featured__item">
                          
                            <div class="featured__item__pic set-bg" data-setbg="{{ URL::to('/') }}/img/<?php echo(explode("*",$product->pic)[1]); ?>.jpg">
                               
                            
                                {{-- <ul class="featured__item__pic__hover">
                                    <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
                                </ul> --}}
                            
                            </div>
                        
                            <div class="featured__item__text">
                                <h6>{{$product->name}}</h6>
                                <h5 style="line-height: 10px;color: #5d2773;">{{MPCO\EnglishPersianNumber\Numbers::toPersianNumbers(DB::table('big_s_products_type')->where("ProductId","=",$product->productID)->orderBy('Price' , 'asc')->get()[0]->Price,3)}} تومان</h5>
                            </div>
                        
                        </div>
                    </a>
                    </div>
            
                    @endforeach

                </div>
            </div>
        </section>
        <!-- Featured Section End -->
@endsection