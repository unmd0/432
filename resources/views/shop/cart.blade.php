@extends('shop.master.index')
@section('shop-main')
<section class="shoping-cart spad hide" id="main">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="shoping__cart__table">
                    <div id="emptycart" class="hide">
                        <div class="col-lg-12">
                            <div class="section-title">
                                <h3>سبد کالا خالی میباشد</h3>
                            </div>
                            <div class="section-title">
                                <a href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}"><h5 style="color: #538ce0 !important;">محصولات</h5></a>
                            </div>
                        </div>
                    </div>
                    <table id="fillcart" class="hide">
                        <thead>
                            <tr>
                                <th class="shoping__product">محصول</th>
                                <th>قیمت</th>
                                <th>تعداد</th>
                                <th>قیمت کل</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            
                                @foreach ($carts as $item)
                                <tr>
                                <td class="shoping__cart__item">
                                    <img src="{{$item->options->pic}}" alt=""><br class="br" style="display: none;"><br class="br" style="display: none;">
                                    <h5>{{$item->name}} - {{$item->options->type->Name}}</h5>
                                </td>
                                <td class="shoping__cart__price" style="text-align: bottom;">
                                    {{MPCO\EnglishPersianNumber\Numbers::toPersianNumbers($item->price,3)}}
                                </td>
                                <td class="shoping__cart__quantity">
                                    <div class="quantity">
                                    <div class="pro-qty" data-id="{{$item->rowId}}">
                                            <span class="dec qtybtn" >-</span>
                                            <input type="text" value="{{$item->qty}}">
                                            <span class="inc qtybtn">+</span>
                                        </div>
                                    </div>
                                </td>
                                <td class="shoping__cart__total" id="{{$item->rowId}}">
                                    {{MPCO\EnglishPersianNumber\Numbers::toPersianNumbers($item->subtotal,3)}}
                                </td>
                                <td class="shoping__cart__item__close" onclick="deletecart(this,'{{$item->rowId}}')">
                                    <span class="icon_close"></span>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            {{-- <div class="col-lg-12">
                <div class="shoping__cart__btns">
                    <a href="#" class="primary-btn cart-btn">ادامه خرید</a>
                    <a href="#" class="primary-btn cart-btn cart-btn-right"><span class="icon_loading"></span>
                        Upadate Cart</a>
                </div>
            </div> --}}
            {{-- <div class="col-lg-6">
                <div class="shoping__continue">
                    <div class="shoping__discount">
                        <h5>Discount Codes</h5>
                        <form action="#">
                            <input type="text" placeholder="Enter your coupon code">
                            <button type="submit" class="site-btn">APPLY COUPON</button>
                        </form>
                    </div>
                </div>
            </div> --}}
            <div class="col-lg-6">
                <div class="shoping__checkout hide" id="checkoutf">
                    <h5>سبد کالا</h5>
                    <ul>
                        <li>قیمت مجموع <span class="price" id="subtotal">{{MPCO\EnglishPersianNumber\Numbers::toPersianNumbers(Cart::subtotal(0,0,""),3)}}</span><span>تومان</span></li>
                        {{-- <li>هزینه ارسال :<span class="price">{{number_format("35000")}} </span><span> تومان </span></li>
                        <li>قیمت پایانی :<span class="price">{{number_format("35000")}} </span><span> تومان </span></li> --}}
                    </ul>
                    <a href="/site/{{ Request::segment(2) }}/checkout" class="primary-btn">رفتن به مرحله بعد</a>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $( document ).ready(function() {

     emptycart({{Cart::count()}});
     
});
</script>
@endsection
