@extends('shop.master.index')
@section('shop-main')
    <!-- Product Details Section Begin -->
    <section class="product-details spad hide" id="main">
        <div class="container">
            <div class="row">
            <span id="idhere">{{$id}}</span>
            <span id="Tidhere">{{$types[0]->TypeId}}</span>    
            <span id="pichere">{{ URL::to('/') }}/img/<?php echo(explode("*",$products->pic)[1]); ?>.jpg</span>
                <div class="col-lg-6 col-md-6" style="overflow: hidden;">
                    <!-- قسمت عکس ها -->
                    <div class="product__details__pic">
                        <!-- عکس اصلی -->
                        <div class="product__details__pic__item">
                            <img class="product__details__pic__item--large"
                                src="{{ URL::to('/') }}/img/<?php echo(explode("*",$products->pic)[1]); ?>.jpg" alt="">
                        </div>
                        <!-- عکس اصلی -->
                        <!-- عکس ها -->
                        <div class="product__details__pic__slider owl-carousel">
                  @foreach (explode(",",str_replace("*","",$products->pic)) as $item)
                  <img data-imgbigurl="{{ URL::to('/')}}/img/{{$item}}.jpg"
                  src="{{ URL::to('/')}}/img/{{$item}}.jpg" alt="">
                  @endforeach

                        </div>
                        <!-- عکس ها -->
                    </div>
                </div>



                
                <div class="col-lg-6 col-md-6">
                    <div class="product__details__text">
                        <h3 id="name">{{$products->name}}</h3>
                        <div class="product__details__price"><span id="price">{{MPCO\EnglishPersianNumber\Numbers::toPersianNumbers($types[0]->Price,3)}}</span> تومان</div>
                        <p>توضیحات محصول</p>
                        <div class="product__details__quantity">
                            <div class="quantity">
                                <div class="pro-qty">
                                    <span class="dec qtybtn" >-</span>
                                    <input type="text" id="qty" value="1">
                                    <span class="inc qtybtn">+</span>
                                </div>
                            </div>
                        </div>
                        <a onclick="addcart(this)" class="primary-btn">اضافه کردن به سبد خرید</a>
                        <ul>

                            <!-- <li><b>Availability</b> <span>In Stock</span></li> -->
                            <!-- <li><b>Shipping</b> <span>01 day shipping. <samp>Free pickup today</samp></span></li>
                            <li><b>Weight</b> <span>0.5 kg</span></li> -->
                        </ul>
                        <div class="form-group">
                            <label for="sel1">انتخاب تنوع:</label>
                            <select  onchange="typec(this)"  class="form-control" id="sel1">
                              @foreach ($types as $type)
                            <option data-price="{{$type->Price}}" data-id="{{$type->TypeId}}">{{$type->Name}}</option>
                              @endforeach
                            </select>
                          </div>
                    </div>
                </div>
                <div class="col-lg-12">

                </div>
            </div>
        </div>
    </section>
    <!-- Product Details Section End -->
<script>
    function typec(x)
    {
        document.getElementById("price").innerHTML=$(x).find(':selected').data('price');
        document.getElementById("Tidhere").innerHTML=$(x).find(':selected').data('id');
    }
    function addcart(X){
        var idj=document.getElementById('idhere').innerHTML;
        var tidj=document.getElementById('Tidhere').innerHTML;
        var picj=document.getElementById('pichere').innerHTML;
        var namej = document.getElementById('name').innerHTML;
        var pricej = document.getElementById('price').innerHTML;
        var qtyj = document.getElementById('qty').value;
      pricej=toEnglishDigits(pricej.replace(",",""));
        var myJsonData = { "_token": "{{ csrf_token() }}", id: idj, tid: tidj,name: namej, price: pricej, qty: qtyj,pic: picj }
    $.post('{{url()->current()}}/addcart', myJsonData, function(response) {
    document.getElementById("cartcount").innerHTML=toPersianNum(response);
});
        
    }
    function toEnglishDigits(str) {

// convert persian digits [۰۱۲۳۴۵۶۷۸۹]
var e = '۰'.charCodeAt(0);
str = str.replace(/[۰-۹]/g, function(t) {
    return t.charCodeAt(0) - e;
});

// convert arabic indic digits [٠١٢٣٤٥٦٧٨٩]
e = '٠'.charCodeAt(0);
str = str.replace(/[٠-٩]/g, function(t) {
    return t.charCodeAt(0) - e;
});
return str;
}
</script>
    @endsection