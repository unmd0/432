@extends('auth.master.index')

@section('main-admin')
    

<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <div class="login100-pic js-tilt" data-tilt>
                <img src="/img/login/img-01.png" alt="IMG">
            </div>
            <form method="POST" action="{{ route('login') }}" class="login100-form validate-form">
                @csrf
                <span class="login100-form-title">
                    ورود مدیران
                </span>
                @error('email')
                <span class="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            @error('password')
            <span class="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
                <div class="wrap-input100 validate-input" data-validate = "لطفا ایمیل را به درستی وارد نمایید: ex@abc.xyz">
                    <input class="input100 @error('email') is-invalid @enderror" type="text" name="email" placeholder="ایمیل"  autocomplete="email" autofocus value="{{ old('email') }}">
                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
                        <i class="fa fa-envelope" aria-hidden="true"></i>
                    </span>

                </div>

                <div class="wrap-input100 validate-input" data-validate = "لطفا یک رمز انتخاب کنید">
                    <input class="input100  " type="password" name="password"  placeholder="رمز عبور" autocomplete="current-password">
                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
                        <i class="fa fa-lock" aria-hidden="true"></i>
                    </span>
                </div>
                
                <div class="container-login100-form-btn">
                    <button class="login100-form-btn">
                        ورود
                    </button>
                </div>

                <div class="text-center p-t-12">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                        <label class="form-check-label" for="remember">
                            مرا به خاطر داشته باش
                        </label>
                    </div><br/>
                    <span class="txt1">
                        فراموشی
                    </span>
                    @if (Route::has('password.request'))
                    <a class="txt2" href="{{ route('password.request') }}">
                        رمز عبور
                    </a>
                @endif
                </div>

                <div class="text-center p-t-136">
                    <a class="txt2" href="{{route('register')}}">
                        ثبت نام در سامانه
                        <i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection


