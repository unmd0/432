@extends('auth.master.index')

@section('main-admin')
    

<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100" style="padding: 33px 130px 33px 95px !important;">
            <div class="login100-pic js-tilt" data-tilt>
                <img src="/img/login/img-01.png" alt="IMG">
            </div>
            <form method="POST" action="{{ route('register') }}" class="login100-form validate-form">
                @csrf
                <span class="login100-form-title">
                    ثبت فروشگاه
                </span>
                @error('email')
                <span class="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            @error('name')
            <span class="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
            @error('password')
            <span class="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
            @error('name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
        <div class="wrap-input100 validate-input" data-validate = "لطفایک نام انتخاب کنید">
            <input class="input100" type="text" name="name" placeholder="نام فروشگاه به انگلیسی" value="{{ old('name') }}" autocomplete="name">
            <span class="focus-input100"></span>

        </div>
                <div class="wrap-input100 validate-input" data-validate = "لطفا ایمیل را به درستی وارد نمایید: ex@abc.xyz">
                    <input class="input100 @error('email') is-invalid @enderror" type="text" name="email" placeholder="ایمیل"  autocomplete="email"  value="{{ old('email') }}">
                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
                        <i class="fa fa-envelope" aria-hidden="true"></i>
                    </span>

                </div>

                <div class="wrap-input100 validate-input" data-validate = "لطفا یک رمز انتخاب کنید">
                    <input class="input100  " type="password" name="password"  placeholder="رمز عبور" autocomplete="current-password">
                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
                        <i class="fa fa-lock" aria-hidden="true"></i>
                    </span>
                </div>

                <div class="wrap-input100 validate-input" data-validate = "لطفا یک رمز انتخاب کنید">
                    <input class="input100  " id="password-confirm" type="password" placeholder="تکرار رمز عبور" name="password_confirmation" autocomplete="new-password">
                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
                        <i class="fa fa-lock" aria-hidden="true"></i>
                    </span>
                </div>
                
                <div class="container-login100-form-btn">
                    <button class="login100-form-btn">
                        ثبت نام
                    </button>
                </div>

                <div class="text-center p-t-12">
                    <span class="txt1">
                        قبلا ثبت نام کرده اید؟ 
                    </span>
                    <a class="txt2" href="{{ route('login') }}">
                        ورود
                    </a>

                </div>

            </form>
        </div>
    </div>
</div>
@endsection
{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> --}}

