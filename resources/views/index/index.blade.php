<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>داشبورد</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link rel="stylesheet" href="/css/fonts/index/icomoon/style.css">

    <link rel="stylesheet" href="/css/index/bootstrap.min.css">
    <link rel="stylesheet" href="/css/index/jquery-ui.css">
    <link rel="stylesheet" href="/css/index/owl.carousel.min.css">
    <link rel="stylesheet" href="/css/index/owl.theme.default.min.css">
    <link rel="stylesheet" href="/css/index/owl.theme.default.min.css">

    <link rel="stylesheet" href="/css/index/jquery.fancybox.min.css">

    <link rel="stylesheet" href="/css/index/bootstrap-datepicker.css">

    <link rel="stylesheet" href="/css/fonts/index/flaticon/font/flaticon.css">

    <link rel="stylesheet" href="/css/index/aos.css">

    <link rel="stylesheet" href="/css/index/style.css">
    
  </head>
  <body style="overflow: hidden;">
  

  <div id="overlayer"></div>
  <div class="loader">
    <div class="spinner-border text-primary" role="status">
      <span class="sr-only">درحال بارگذاری</span>
    </div>
  </div>


  <div class="site-wrap">

    <div class="site-mobile-menu site-navbar-target">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>
   
    
    <header class="site-navbar js-sticky-header site-navbar-target" role="banner" >

      <div class="container">
        <div class="row align-items-center">
          
          <div class="col-6 col-xl-2">
            <h1 class="mb-0 site-logo"><a href="index.html" class="h2 mb-0">۸۱۹۰<span class="text-primary">.</span> </a></h1>
          </div>

          <div class="col-12 col-md-10 d-none d-xl-block">
            <nav class="site-navigation position-relative text-right" role="navigation">

              <ul class="site-menu main-menu js-clone-nav mr-auto d-none d-lg-block rtl">
                <li><a href="#home-section" class="nav-link">خانه</a></li>
                <li><a href="#pricing-section" class="nav-link">تعرفه ها</a></li>
                <li><a href="/login" class="nav-link">ورود</a></li>
                <li><a href="/register" class="nav-link">ثبت نام</a></li>
                <li><a href="#faq-section" class="nav-link">سوالات متداول</a></li>
              </ul>
            </nav>
          </div>


          <div class="col-6 d-inline-block d-xl-none ml-md-0 py-3" style="position: relative; top: 3px;"><a href="#" class="site-menu-toggle js-menu-toggle float-right"><span class="icon-menu h3"></span></a></div>

        </div>
      </div>
      
    </header>

  
    <section class="site-blocks-cover overflow-hidden bg-light" id="home-section">
      <div class="container">
        <div class="row">
          <div class="col-md-7 align-self-center text-center text-md-left">
            <div class="intro-text">
              <h1 style="text-align: center;"><span class="d-md-block" style="color: #ff317d;margin-left: 165px;">فروشگاه <br/> </span>خودتو بساز</h1>
              <p class="mb-4">در کمترین زمان فروشگاه اینترنتی بساز و کسب و کارت رو گسترش بده</p>
            </div>
          </div>
          <div class="col-md-5 align-self-end text-center text-md-right">
            <img src="/img/index/ecommerce-inner-2.png" alt="Image" class="img-fluid cover-img">
          </div>
        </div>
      </div>
    </section> 

    <section class="site-section">
      <div class="container">
        <div class="row justify-content-center" data-aos="fade-up">
          <div class="col-lg-6 text-center heading-section mb-5">
            <h1 class="text-black mb-2"> چرا<h4> باید فروشگاه اینترنتی داشت؟</h4></h1>
            <p></p>
          </div>
        </div>

        <div class="row hover-1-wrap mb-5 mb-lg-0">
          <div class="col-12">
            <div class="row">
              <div class="mb-4 mb-lg-0 col-lg-6 order-lg-2" data-aos="fade-right">
                <a href="#" class="hover-1">
                  <img src="/img/index/istockphoto-1077760200-612xjk612.jpg" alt="Image" class="img-fluid">
                </a>
              </div>
              <div class="col-lg-5 mr-auto text-lg-right align-self-center order-lg-1" data-aos="fade-left">
                <h2 class="text-black">اعتبار کسب و کارت رو ببر بالا</h2>
                <p class="mb-4">با داشتن فروشگاه اینترنتی حرفه ای میتوانید با کمترین هزینه اعتبار کسب و کارت رو ببری بالا</p>
              </div>
            </div>
          </div>
        </div>

        <div class="row hover-1-wrap mb-5 mb-lg-0">
          <div class="col-12">
            <div class="row">
              <div class="mb-4 mb-lg-0 col-lg-6"  data-aos="fade-left">
                <a href="#" class="hover-1">
                  <img src="/img/index/istockphoto-1077760200-612x612.jpg" alt="Image" class="img-fluid">
                </a>
              </div>
              <div class="col-lg-5 ml-auto align-self-center"  data-aos="fade-right">
                <h2 class="text-black">گسترش</h2>
                <p class="mb-4">راه های تعامل و فروش خود را گسترش دهید</p>
              </div>
            </div>
          </div>
        </div>

        <div class="row hover-1-wrap">
          <div class="col-12">
            <div class="row">
              <div class="mb-4 mb-lg-0 col-lg-6 order-lg-2" data-aos="fade-right">
                <a href="#" class="hover-1">
                  <img src="/img/index/istockphoto-1077760200-6''12x612.jpg" alt="Image" class="img-fluid">
                </a>
              </div>
              <div class="col-lg-5 mr-auto text-lg-right align-self-center order-lg-1" data-aos="fade-left">
                <h2 class="text-black">مدیریت ساده فروشگاه</h2>
                <p class="mb-4">پلتفرم 8190 به شما امکان مدیریت فروشگاه خود به ساده ترین روش ممکن میسازد</p>
              </div>
            </div>
          </div>
        </div>

        <div class="row hover-1-wrap mb-5 mb-lg-0">
          <div class="col-12">
            <div class="row">
              <div class="mb-4 mb-lg-0 col-lg-6"  data-aos="fade-left">
                <a href="#" class="hover-1">
                  <img src="/img/index/istockphoto-1077760200-x612.jpg" alt="Image" class="img-fluid">
                </a>
              </div>
              <div class="col-lg-5 ml-auto align-self-center"  data-aos="fade-right">
                <h2 class="text-black">درگاه پرداخت آماده</h2>
                <p class="mb-4">در پلتفرم 8190 شما میتوانید از درگاه پرداخت این سایت استفاده کنید</p>
              </div>
            </div>
          </div>
        </div>



      </div>
    </section>

    <section class="site-section" id="pricing-section">
      <div class="container">
        <div class="row justify-content-center" data-aos="fade-up">
          <div class="col-lg-7 text-center heading-section mb-5">
            <div class="paws">
              <span class="icon-paw"></span>
            </div>
            <h2 class="mb-2 text-black heading">تعرفه ها</h2>
            <p>بهترین قیمت ها</p>
          </div>
        </div>
        <div class="row no-gutters rtl">




          <div class="col-12 col-sm-6 col-md-6 col-lg-4 bg-primary p-3 p-md-5" data-aos="fade-up" data-aos-delay="">
            <div class="pricing">
              <h3 class="text-center text-uppercase">تست</h3>
              <div class="price text-center mb-4 text-black">
                <span class="text-black"><span class="text-black" style="color: #ff046c !important;"> رایگان </span>۶روزه</span>
              </div>
              <!-- <ul class="list-unstyled ul-check success mb-5">
                
                <li>Officia quaerat eaque neque</li>
                <li>Possimus aut consequuntur incidunt</li>
                <li class="remove">Lorem ipsum dolor sit amet</li>
                <li class="remove">Consectetur adipisicing elit</li>
                <li class="remove">Dolorum esse odio quas architecto sint</li>
              </ul> -->
              <p class="text-center">
                <a href="/register/" class="btn btn-secondary">ثبت نام</a>
              </p>
            </div>
          </div>




          <div class="col-12 col-sm-6 col-md-6 col-lg-4 bg-dark  p-3 p-md-5" style="    background-color: #d1e1f5 !important;" data-aos="fade-up" data-aos-delay="100">
            <div class="pricing">
              <h3 class="text-center text-white text-uppercase rtl text-black">۹۶ روزه</h3>
              <div class="price text-center mb-4 rtl text-black">
                <span class="rtl text-black"><span  style="color: #ff046c !important;">۹۶</span> هزار تومان</span>
              </div>
              <!-- <ul class="list-unstyled ul-check success mb-5">
                
                <li>Officia quaerat eaque neque</li>
                <li>Possimus aut consequuntur incidunt</li>
                <li>Lorem ipsum dolor sit amet</li>
                <li>Consectetur adipisicing elit</li>
                <li class="remove">Dolorum esse odio quas architecto sint</li>
              </ul> -->
              <p class="text-center">
                <a href="/register/" class="btn btn-secondary">ثبت نام</a>
              </p>
            </div>
          </div>


          <div class="col-12 col-sm-6 col-md-6 col-lg-4 bg-primary  p-3 p-md-5" style="background-color: #c5daf5 !important;" data-aos="fade-up" data-aos-delay="200">
            <div class="pricing">
              <h3 class="text-center  text-uppercase rtl">۳۶۹ روزه</h3>
              <div class="price text-center mb-4 rtl text-black">
                <span class="rtl text-black"><span class="text-black" style="color: #ff046c !important;" >۳۹۶</span> هزار تومان</span>
              </div>
              <!-- <ul class="list-unstyled ul-check success mb-5">
                
                <li>Officia quaerat eaque neque</li>
                <li>Possimus aut consequuntur incidunt</li>
                <li>Lorem ipsum dolor sit amet</li>
                <li>Consectetur adipisicing elit</li>
                <li>Dolorum esse odio quas architecto sint</li>
              </ul> -->
              <p class="text-center">
                <a href="/register/" class="btn btn-secondary">ثبت نام</a>
              </p>
            </div>
          </div>






        </div>
      </div>
    </section>

    <section class="site-section" id="faq-section">
      <div class="container" id="accordion">
        <div class="row justify-content-center" data-aos="fade-up">
          <div class="col-lg-6 text-center heading-section mb-5">
            <div class="paws">
              <span class="icon-paw"></span>
            </div>
            <h2 class="text-black mb-2">سوالات متداول</h2>
            <!-- <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p> -->
          </div>
        </div>
        <div class="row accordion justify-content-center block__76208">
          <!-- <div class="col-lg-6 order-lg-2 mb-5 mb-lg-0" data-aos="fade-up"  data-aos-delay="">
            <img src="/img/index/dogger_img_sm_1.jpg" alt="Image" class="img-fluid rounded">
          </div> -->
          <div class="col-lg-5 order-lg-1 text-right" data-aos="fade-up"  data-aos-delay="100">
            <div class="accordion-item">
              <h3 class="mb-0 heading text-right">
                <a class="btn-block" data-toggle="collapse" href="#collapseFive" role="button" aria-expanded="true" aria-controls="collapseFive">آیا برای راه اندازی فروشگاه اینترنتی در این پتفرم نیاز به برنامه نویسی و تخصص است؟<span class="icon"></span></a>
              </h3>
              <div id="collapseFive" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="body-text">
                  <p>خیر. راه اندازی فروشگاه در این پلتفرم به سادگی امکان پذیر است و کافیست شما در این سایت ثبت نام کنید و محصولات خود را اضافه کنید تا فروش شما شروع شود</p>
                </div>
              </div>
            </div> <!-- .accordion-item -->

            <div class="accordion-item">
              <h3 class="mb-0 heading">
                <a class="btn-block" data-toggle="collapse" href="#collapseSix" role="button" aria-expanded="false" aria-controls="collapseSix">آیا نیاز به خرید هاست و دامین میباشد؟<span class="icon"></span></a>
              </h3>
              <div id="collapseSix" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="body-text">
                  <p>خیر. هاست و دامین را به ما بسپارید، اما در صورتی که نیاز به دامین اختصاصی خود بودین با ما ارتباط برقرار کنید تا روش انتقال را برای شما انجام دهیم</p>
                </div>
              </div>
            </div> <!-- .accordion-item -->

            <div class="accordion-item">
              <h3 class="mb-0 heading">
                <a class="btn-block" data-toggle="collapse" href="#collapseSeven" role="button" aria-expanded="false" aria-controls="collapseSeven">اگر از درگاه پرداخت این پلتفرم استفاده کنم چگونه پول برای من واریز می شود.<span class="icon"></span></a>
              </h3>
              <div id="collapseSeven" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="body-text">
                  <p>پس از کسر ۴% از مبلغ، کمتر از ۴۸ ساعت برای شما پرداخت می شود</p>
                </div>
              </div>
            </div> <!-- .accordion-item -->

            <!-- <div class="accordion-item">
              <h3 class="mb-0 heading">
                <a class="btn-block" data-toggle="collapse" href="#collapseEight" role="button" aria-expanded="false" aria-controls="collapseEight">Is a warm dry nose a sign of illness in dogs?<span class="icon"></span></a>
              </h3>
              <div id="collapseEight" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="body-text">
                  <p>The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen. She packed her seven versalia, put her initial into the belt and made herself on the way.</p>
                </div>
              </div>
            </div>  -->

          </div>

          
        </div>
      </div>
    </section>

    <!-- <section class="site-section bg-light block-13" id="testimonials-section" data-aos="fade">
      <div class="container">
        
        <div class="row justify-content-center" data-aos="fade-up">
          <div class="col-lg-6 text-center heading-section mb-5">
            <div class="paws">
              <span class="icon-paw"></span>
            </div>
            <h2 class="text-black mb-2">Happy Customers</h2>
          </div>
        </div>
        <div  data-aos="fade-up" data-aos-delay="200">
          <div class="owl-carousel nonloop-block-13">
            <div>
              <div class="block-testimony-1 text-center">
                
                <blockquote class="mb-4">
                  <p>&ldquo;The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen. She packed her seven versalia, put her initial into the belt and made herself on the way.&rdquo;</p>
                </blockquote>

                <figure>
                  <img src="/img/index/person_4.jpg" alt="Image" class="img-fluid rounded-circle mx-auto">
                </figure>
                <h3 class="font-size-20 text-black">Ricky Fisher</h3>
              </div>
            </div>

            <div>
              <div class="block-testimony-1 text-center">

                

                <blockquote class="mb-4">
                  <p>&ldquo;Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.&rdquo;</p>
                </blockquote>

                <figure>
                  <img src="/img/index/person_2.jpg" alt="Image" class="img-fluid rounded-circle mx-auto">
                </figure>
                <h3 class="font-size-20 mb-4 text-black">Ken Davis</h3>

                
              </div>
            </div>

            <div>
              <div class="block-testimony-1 text-center">
                

                <blockquote class="mb-4">
                  <p>&ldquo;A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.&rdquo;</p>
                </blockquote>

                <figure>
                  <img src="/img/index/person_1.jpg" alt="Image" class="img-fluid rounded-circle mx-auto">
                </figure>
                <h3 class="font-size-20 text-black">Mellisa Griffin</h3>

                
              </div>
            </div>

            <div>
              <div class="block-testimony-1 text-center">

                

                <blockquote class="mb-4">
                  <p>&ldquo;Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.&rdquo;</p>
                </blockquote>

                <figure>
                  <img src="/img/index/person_3.jpg" alt="Image" class="img-fluid rounded-circle mx-auto">
                </figure>
                <h3 class="font-size-20 mb-4 text-black">Robert Steward</h3>

                
              </div>
            </div>


          </div>
        </div>
      </div>
    </section> -->

    
    <footer class="site-footer">
      <div class="container">
        <div class="row">
          <div class="col-md-9">
            <div class="row">
              <div class="col-md-5">
                <h2 class="footer-heading mb-4">درباره ما</h2>
                <p class="text-right">پلتفرم ۸۱۹۰ با استفاده از بهترین و بروز ترین سیستم ها آماده ارایه خدمات طراحی سایت های فروشگاهی به صورت اتوماتیک و سئو شده می باشد</p>
              </div>
              <div class="col-md-3 ml-auto">
                <!-- <h2 class="footer-heading mb-4">Quick Links</h2>
                <ul class="list-unstyled">
                  <li><a href="#about-section" class="smoothscroll">About Us</a></li>
                  <li><a href="#trainers-section" class="smoothscroll">Trainers</a></li>
                  <li><a href="#services-section" class="smoothscroll">Services</a></li>
                  <li><a href="#testimonials-section" class="smoothscroll">Testimonials</a></li>
                  <li><a href="#contact-section" class="smoothscroll">Contact Us</a></li>
                </ul> -->
              </div>
              <div class="col-md-3">
                
                <!-- <a href="#" class="pl-0 pr-3"><span class="icon-facebook"></span></a>
                <a href="#" class="pl-3 pr-3"><span class="icon-twitter"></span></a> -->
                
                <!-- <a href="#" class="pl-3 pr-3"><span class="icon-linkedin"></span></a> -->
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <h2 class="footer-heading mb-4">مارا دنبال کنید</h2>
            <a href="https://www.instagram.com/unmd0/" class="pl-3 pr-3"><span class="icon-instagram"></span></a>
            <!-- <h2 class="footer-heading mb-4">Subscribe Newsletter</h2>
            <form action="#" method="post" class="footer-subscribe">
              <div class="input-group mb-3">
                <input type="text" class="form-control border-secondary text-white bg-transparent" placeholder="Enter Email" aria-label="Enter Email" aria-describedby="button-addon2">
                <div class="input-group-append">
                  <button class="btn btn-primary text-black" type="button" id="button-addon2">Send</button>
                </div>
              </div>
            </form> -->
          </div>
        </div>
        <div class="row pt-5 mt-5 text-center">
          <div class="col-md-12">
            <div class="border-top pt-5">
              <p class="copyright" style="direction: rtl;"small>
              <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            ۱۳۹۹ ساخته شده با <i class="icon-heart text-danger" aria-hidden="true"></i> بدست تیم <a href="#home-section" >۸۱۹۰</a>
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></small></p>
        
            </div>
          </div>
          
        </div>
      </div>
    </footer>

  </div> <!-- .site-wrap -->

  <script src="/js/index/jquery-3.3.1.min.js"></script>
  <script src="/js/index/jquery-ui.js"></script>
  <script src="/js/index/popper.min.js"></script>
  <script src="/js/index/bootstrap.min.js"></script>
  <script src="/js/index/owl.carousel.min.js"></script>
  <script src="/js/index/jquery.countdown.min.js"></script>
  <script src="/js/index/jquery.easing.1.3.js"></script>
  <script src="/js/index/aos.js"></script>
  <script src="/js/index/jquery.fancybox.min.js"></script>
  <script src="/js/index/jquery.sticky.js"></script>
  <script src="/js/index/isotope.pkgd.min.js"></script>

  
  <script src="/js/index/main.js"></script>

  
  </body>
</html>