@extends('admin.master.index')
@section('admin-main')
<main class="c-main c-main-padd">
    <div class="uk-container uk-container-large">
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif

        <form action="/admin/profile/update" method="post">
@csrf
@method("PUT")
            <div class="form-group">
                <label for="name">نام فروشگاه</label>
                <input type="text" name="name" value="{{$profile->name}}" class="form-control">
            </div>
            <hr/>
            {{-- <div class="form-group">
                <label for="desc">توضیحات</label>
                <textarea name="desc" id="desc" cols="30" rows="10" class="form-control"></textarea>
            </div> --}}
        <input type="text" value="{{Auth::user()->name}}" class="hide" id="namehere">
            <div class="form-group">
                <label for="category">اینستاگرام</label>
                <input type="text" name="instagram" value="{{$profile->instagram}}" placeholder="" class="form-control instagraminp">
            </div>
            <div class="form-group">
                <label for="category">تلگرام</label>
                <input type="text" name="telegram" value="{{$profile->telegram}}" class="form-control telegraminp">
            </div>
            {{-- <div class="form-group">
                <label for="category">دسته بندی</label>
                <input type="text" name="category" class="form-control whatsappinp">
            </div> --}}
            <hr/>
            <div class="form-group">
                <output id="list">
                    @if ($profile->logo)
                    <span class="">
                        <img class="thum" src="{{ URL::to('/') }}/img/logo/img{{$profile->logo}}.png" title="{{$profile->logo}}">
                        </span>
                    @endif

                </output>
            </div>
            <div class="form-group">
                <input type="file" class="hide" id="btn_photo"  accept=".png">
                <button type="button" class="btn btn-info" onclick="photo_select()">بارگذاری لوگو</button>
                <input type="text" id="pics1" name="logo" class="hide">
            </div>
            <hr/>
            <div class="form-group">
                <output id="list2">
                    @if ($profile->validatepic)
                    <span class="">
                        <img class="thum" src="{{ URL::to('/') }}/img/validate/{{$profile->validatepic}}.jpg" title="{{$profile->validatepic}}">
                        </span>
                    @endif

                </output>
                <div class="form-group">
                    <input type="file" class="hide" id="btn_photo2"   accept=".jpg">
                    <button type="button" class="btn btn-info" onclick="photo_select2()">بارگذاری کارت ملی</button>
                    <input type="text" id="pics2" name="validatepic" class="hide">
                </div>
            </div>
            <hr/>
            <div class="form-group">
                <label for="keywords">آدرس</label>
                <input type="text" name="address" value="{{$profile->address}}" class="form-control">
            </div>
            <div class="form-group">
                <label for="keywords">شماره تماس</label>
                <input type="text" name="phone" class="form-control whatsappinp" value="{{$profile->phone}}" style="background: white">
            </div>
            <div class="form-group">
                <label for="keywords"> شماره شبا </label><br/>
                <input type="text" name="irshaba" class="form-control whatsappinp" value="{{$profile->irshaba}}" style="background: white;display: inline !important;"> IR 
            </div>
            <button type="button" class="btn btn-info" onclick="subm()">ثبت تغییرات</button>
            <button type="submit" class="hide" id="subp"></button>
        </form>
    </div>



</main>
<script src="/js/logoupload.js"></script> 
@endsection