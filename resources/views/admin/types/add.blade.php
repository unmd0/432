@extends('admin.master.index')
@section('admin-main')
<script src="/js/addtype.js"></script>
<main class="c-main">
<div class="uk-container uk-container-large">
    <div id="typs">
        <input type="text" class="hide" id="idhere" value={{$ID}}>
    @if ($typeAll)
        @foreach ($typeAll as $item)
        <div class="c-cart1">




            <div class="form-group">
                <label for="title">نام تنوع</label>
            <input type="text" name="name" disabled class="form-control" value="{{$item->Name}}">
            </div>
            <div class="form-group">
                <label for="body">قیمت</label>
                <input type="text" name="price" disabled class="form-control" value="{{$item->Price}}">
            </div>
            <div class="form-group">
                <input type="checkbox" disabled @if ($item->Avaliable==1) checked @endif style="margin-right: 5px !important;position:relative;">
                <label for="customControlInline">موجود</label>
            </div>
            <div class="form-group"> 
                <button class="btn btn-success" onclick="editmebtn(this)">ویرایش</button>
                <input type="text" class="hide" value="{{$item->TypeId}}">
            </div>

        

    </div>
        @endforeach
    @endif
    <div class="c-cart1">




            <div class="form-group">
                <label for="title">نام تنوع</label>
                <input type="text" name="name" class="form-control">
            </div>
            <div class="form-group">
                <label for="body">قیمت</label>
                <input type="text" name="price" class="form-control">
            </div>
            <div class="form-group">
                <input type="checkbox" checked style="margin-right: 5px !important;position:relative;">
                <label for="customControlInline">موجود</label>
            </div>
            <div class="form-group"> 
                <button class="btn btn-success" onclick="saveme(this)">ذخیره</button><input type="text" class="hide">
            </div>
 



        

    </div>
    </div>
    <button type="button" class="btn btn-info" onclick="subm2()"> اضافه کردن تنوع جدید </button>
    <a href="/admin/type/show"><button type="button" class="btn btn-info">پایان</button></a>
    <button type="submit" class="hide"></button>
</div>
</main>

@endsection