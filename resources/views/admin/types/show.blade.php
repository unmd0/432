@extends('admin.master.index')
@section('meta_add')
<meta name="viewport" content="width=device-width, initial-scale=0.5">
@endsection
@section('admin-main')
<main class="c-main c-main-padd">
<div class="uk-container uk-container-large">
    <div class="div-padd">
        <form action="/admin/type/search" method="GET">
        <input type="text" name="search1" value="{{$q ?? ''}}" class="search-inp">
            <button type="submit" class="btn btn-success">جستجو</button>
            <a href="/admin/type/show">
            <button type="button" class="btn btn-danger">
                <span>&times;</span>
              </button>
            </a>
    </form>



</div>
    <table class="table">
        <thead>
            <tr>
                <th> </th>
                <th>کد تنوع</th>
                <th>نام</th>
                <th>قیمت</th>
                <th>موجود</th>
                <th>عملیات</th>
            </tr>
        </thead>
        <tbody>
            @foreach($types as $type)
                <tr>
                    <td><img src="{{ URL::to('/') }}/img/<?php echo(explode("*",$type->pic)[1]); ?>.jpg" alt="" class="thum"></td>
                    <td>{{ $type->TypeId }}</td>
                    <td>{{ $type->name }} - {{ $type->Name }} </td>
                    <td>{{ $type->Price }}</td>
                    <td>{{ $type->Avaliable }}</td>
                    <td>
                        {{-- <button class="btn btn-danger">delete</button> --}}
                   
                           <a href="/admin/type/create/{{$type->ProductId}}">
                         <button type="submit" class="btn btn-green">ویرایش</button>
                        </a>
                     
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
<div>
    {{$types->links()}}
</div>

</div>
</main>

@endsection