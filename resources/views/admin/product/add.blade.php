@extends('admin.master.index')
@section('admin-main')
<main class="c-main c-main-padd">



    <div class="uk-container uk-container-large">
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="/admin/product/create" method="post">
@csrf
@method("PUT")
            <div class="form-group">
                <label for="name">نام محصول</label>
                <input type="text" name="name" class="form-control">
            </div>
            <div class="form-group">
                <label for="desc">توضیحات</label>
                <textarea name="desc" id="desc" cols="30" rows="10" class="form-control"></textarea>
            </div>
            <div class="form-group">
                <label for="category">دسته بندی</label>
                <input type="text" name="category" class="form-control">
            </div>
            <div class="form-group">
                <input type="file" multiple class="hide" id="btn_photo"   accept=".jpg">
                <button type="button" class="btn btn-info" onclick="photo_select()">افزودن عکس</button>
                <input type="text" id="pics1" name="pic" class="hide">
            </div>
            <div class="form-group">
                <output id="list"></output>
            </div>
            <div class="form-group">
                <label for="keywords">کلمات کلیدی (جداسازی با ، )</label>
                <input type="text" name="keywords" class="form-control">
            </div>

            <button type="button" class="btn btn-info" onclick="subm()">ثبت محصول و رفتن به مرحله بعد</button>
            <button type="submit" class="hide" id="subp"></button>
        </form>
    </div>



</main>
@endsection