@extends('admin.master.index')
@section('admin-main')
<main class="c-main c-main-padd">



    <div class="uk-container uk-container-large">
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="/admin/product/edit" method="post">
@csrf
@method("PUT")
<input type="text" name="productID" value="{{$types->productID}}"  class="hide">
            <div class="form-group">
                <label for="name">نام محصول</label>
                <input type="text" name="name" value="{{$types->name}}" class="form-control">
            </div>
            <div class="form-group">
                <label for="desc">توضیحات</label>
                <textarea name="desc" id="desc" cols="30" rows="10" class="form-control">{{$types->desc}}</textarea>
            </div>
            <div class="form-group">
                <label for="category">دسته بندی</label>
  <select class="ui search dropdown" name="category">
                @foreach ($category as $item)
                <option value="{{$item->cname}}" {{$item->cname == $types->category ? "selected" :"" }}>{{$item->cname}}</option>
                @endforeach
  </select>
            </div>
            <div class="form-group">
                <input type="file" multiple class="hide" id="btn_photo"  accept=".jpg">
                <button type="button" class="btn btn-info" onclick="photo_select()">افزودن عکس</button>
                <input type="text" id="pics1" name="pic" class="hide">
            </div>
            <div class="form-group">
                <output id="list">
                    <span class="picture_preview c-grid__col width-med select-main-pic">
                    <img class="thum" src="{{ URL::to('/') }}/img/<?php echo(explode("*",$types->pic)[1]); ?>.jpg" title="<?php echo(explode("*",$types->pic)[1]); ?>">
                        <button type="button" class="btn btn-red btn-med" onclick="deleteme1(this)">حذف</button> 
                        <button type="button" class="btn btn-green btn-med" onclick="selectme2(this)">عکس اصلی</button>
                    </span>
                    <?php $pics =explode(",",$types->pic) ?>
                    @foreach ($pics as $item)
                    @if (!Str::contains($item,"*"))
                    <span class="picture_preview c-grid__col width-med">
                        <img class="thum" src="{{ URL::to('/') }}/img/{{$item}}.jpg" title="{{$item}}">
                            <button type="button" class="btn btn-red btn-med" onclick="deleteme1(this)">حذف</button> 
                            <button type="button" class="btn btn-green btn-med" onclick="selectme2(this)">عکس اصلی</button>
                        </span>
                        @endif
                    @endforeach
                </output>
            </div>
            <div class="form-group">
                <label for="keywords">کلمات کلیدی (جداسازی با ، )</label>
                <input type="text" name="keywords" value="{{$types->keywords}}" class="form-control">
            </div>

            <button type="button" class="btn btn-info" onclick="subm()">ثبت محصول و رفتن به مرحله بعد</button>
            <button type="submit" class="hide" id="subp"></button>
        </form>
    </div>



</main>
@endsection