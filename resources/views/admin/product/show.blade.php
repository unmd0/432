@extends('admin.master.index')
@section('meta_add')
<meta name="viewport" content="width=device-width, initial-scale=0.5">
@endsection
@section('admin-main')
<main class="c-main c-main-padd">
<div class="uk-container uk-container-large">
    <div class="div-padd">
        <form action="/admin/product/search" method="GET">
        <input type="text" name="search1" value="{{$q ?? ''}}" class="search-inp">
            <button type="submit" class="btn btn-success">جستجو</button>
            <a href="/admin/product/show">
            <button type="button" class="btn btn-danger">
                <span>&times;</span>
              </button>
            </a>
    </form>



</div>
    <table class="table">
        <thead>
            <tr>
                <th>ردیف</th>
                <th> </th>
                <th>کد محصول</th>
                <th>نام</th>
                <th>دسته بندی</th>
                <th>عملیات</th>
            </tr>
        </thead>
        <tbody>
            <?php $i=1 ?>
            @foreach($types as $type)
                <tr>
                    <td><?php echo($i); $i=$i + 1; ?></td>
                    <td><img src="{{ URL::to('/') }}/img/<?php echo(explode("*",$type->pic)[1]); ?>.jpg" alt="" class="thum"></td>
                    <td>{{ $type->productID }}</td>
                    <td> {{ $type->name }} </td>
                    <td>{{ $type->category }}</td>
                    <td>
                        {{-- <button class="btn btn-danger">delete</button> --}}
                   
                           <a href="/admin/product/edit/{{$type->productID}}">
                         <button type="submit" class="btn btn-green">ویرایش محصول</button>
                        </a>
                        <br/>
                        <br/>
                        <a href="/admin/type/create/{{$type->productID}}">
                            <button type="submit" class="btn btn-info">اضافه کردن تنوع</button>
                           </a>
                     
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
<div>
    {{$types->links()}}
</div>

</div>
</main>


@endsection