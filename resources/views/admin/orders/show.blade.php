@extends('admin.master.index')
@section('meta_add')
<meta name="viewport" content="width=device-width, initial-scale=0.5">
@endsection
@section('admin-main')
<main class="c-main c-main-padd">
<div class="uk-container uk-container-large">
    <div class="div-padd">
        
    </div>
    <table class="table">
        <thead>
            <tr>
                <th>ردیف</th>
                <th>کد سفارش</th>
                <th>مبلغ</th>
                <th>وضعیت</th>
                <th>تاریخ سفارش</th>
                <th>عملیات</th>
            </tr>
        </thead>
        <tbody>
            <?php $i=1 ?>

            @foreach($orders as $item)
                    <tr class=@if($item->shopstatus == 0) {{"neworder"}} @endif>
                    <td><?php echo($i); $i=$i + 1; ?></td>
                    <td>{{ $item->id }}</td>
                    <td> {{ $item->price }} تومان </td>
                    <td>{{ $item->status }}</td>
                    <td style="direction: ltr;">{{ \Morilog\Jalali\CalendarUtils::jalalicreated($item) }}</td>
                    <td>
                        {{-- <button class="btn btn-danger">delete</button> --}}
                            
                           <a href="/admin/orders/{{$item->id}}">
                         <button type="submit" class="btn btn-green">مشاهده جزئیات</button>
                        </a>
                     
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
<div>
    {{$orders->links()}}
</div>

</div>
</main>


@endsection