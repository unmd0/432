@extends('admin.master.index')
@section('meta_add')
<meta name="viewport" content="width=device-width, initial-scale=0.5">
@endsection
@section('admin-main')
<main class="c-main c-main-padd">
<div class="uk-container uk-container-large">
    @if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
    <div class="div-padd">
        <a href="{{ url('admin/orders') }}">بازگشت</a><br/><br/>
        <span>توضیحات:  <td>{{ $orders[0]->desc? $orders[0]->desc :"خالی" }}</td></span><br/><br/>
        <span>آدرس:  <td>{{ $orders[0]->state }}-{{ $orders[0]->city }}، {{ $orders[0]->address }}  -  شماره تماس: {{ $orders[0]->phone }}</td></span>
    </div>
    <table class="table">
        <thead>
            <tr>
                <th>ردیف</th>
                <th></th>
                <th>نام محصول</th>
                <th>مبلغ</th>
                <th>تعداد</th>
                <th>مبلغ کل</th>
                <th>عملیات</th>
            </tr>
        </thead>
        <tbody>
            <?php $i=1 ?>

            @foreach($orders as $item)
                <tr>
                    <td><?php echo($i); $i=$i + 1; ?></td>
                    <td><img src="{{$item->pic}}" alt="" class="thum"></td>
                    <td>{{ $item->name }}</td>
                    <td> {{ $item->priceeach }} تومان </td>
                    <td>{{ $item->qty }}</td>
                    <td>{{ $item->subtotal }} تومان </td>
                    <td>
                        {{-- <a href="/admin/product/edit/{{$item->productID}}"> --}}
                            @if ($item->shopstatus <> "2")
                            <a href="/admin/orders/ship/{{$item->id}}">
                                <button type="submit" class="btn btn-green">آماده ارسال</button>
                             </a>
                                 
                             @else
                             آماده ارسال
                            @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
<div>
    {{$orders->links()}}
</div>

</div>
</main>


@endsection