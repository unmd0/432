@extends('admin.master.index')
@section('admin-main')
<script src="/js/addcategory.js"></script>
<main class="c-main" style="padding-top:20px;">
    <div class="uk-container uk-container-large">
        <input type="text" class="hide" id="idhere" value="{{$id}}">
        @isset($cname1[0])
        <a href="/admin/category/{{$cname1[0]->pid}}">{{$cname1[0]->cname}}</a>
        @endisset
      
        

        <div id="menus">
            @foreach ($categorys as $category)
            <div class="c-cart1">
                <div class="form-group row">
                    <label for="staticEmail" class="col-sm-1">نام دسته</label>
                    <div class="col-sm-5">
                    <input type="text" value="{{$category->cname}}" disabled class="form-control-plaintext" value="">
                        </div>
                        <div class="col-sm-6"> 
                        <input type="text" class="hide" value="{{$category->id}}"> 
                            <button type="button" class="btn btn-info" onclick="editmebtn(this)">ویرایش</button> 
                            <a href="/admin/category/{{$category->id}}">
                                <button type="button" class="btn btn-green">اضافه کردن زیر دسته</button>
                            </a>
                            <button type="button" class="btn btn-red" onclick="deleteme(this)">حذف</button>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="c-cart1">
                <div class="form-group row">
                    <label for="staticEmail" class="col-sm-1">نام دسته</label>
                    <div class="col-sm-5">
                      <input type="text" class="form-control" id="newname" value="">
                      <!-- class="form-control-plaintext" -->
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="hide">
                        <button type="button" class="btn btn-info" onclick="saveme(this)">ذخیره دسته</button>
                        <button type="button" class="btn btn-green hide" onclick="submenu()">اضافه کردن زیر دسته</button>
                        <button type="button" class="btn btn-red hide" onclick="deleteme()">حذف</button>
                    </div>
                  </div>
                <button type="submit" class="hide">ثبت محصول</button>
        </div>
        
    </div>
</main>
@endsection