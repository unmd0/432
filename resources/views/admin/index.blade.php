@extends('admin.master.index')
@section('admin-main')
<main class="c-main">
    <div class="uk-container uk-container-large">



        <div class="c-grid__row">


            <div class="c-grid__col c-grid__col--lg-4" style="padding-top: 5%;">
                <div class="c-card">
                    <div class="c-card__header">
                        <h2 class="c-card__title">سفارشات</h2>
                    </div>
                    <div class="c-card__body">


                        <div class="c-card__stat c-card__stat--section">
                            <a class="#" href="{{ url('admin/orders') }}">
                                <div class="c-card__stat-value c-card__stat-value--active">
                                    <span dir="ltr">{{DB::table('orders')->where('created_at', '>=', \Carbon\Carbon::today())->count()}}</span>
                                </div>
                                <p class="c-card__stat-description">سفارشات امروز</p>
                            </a>
                        </div>


                        <a class="c-card__stat js-change-selling-chart" href="{{ url('admin/orders') }}">
                            <div class="c-card__stat-value" data-value="137897000">
                                <span dir="ltr">{{DB::table('orders')->where("shopstatus","=","0")->count()}}</span>
                            </div>
                            <p class="c-card__stat-description">سفارشات مشاهد نشده</p>
                        </a>

                        <a class="c-card__stat js-change-selling-chart" href="{{ url('admin/orders') }}">
                            <div class="c-card__stat-value">
                                <span dir="ltr">{{DB::table('orders')->count()}}</span>
                            </div>
                            <p class="c-card__stat-description">سفارشات تا به امروز</p>
                        </a>
                    </div>
                </div>
            </div>

            <div class="c-grid__col c-grid__col--lg-4" style="padding-top: 5%;">
                <div class="c-card">
                    <div class="c-card__header">
                        <h2 class="c-card__title">محصولات</h2>
                    </div>
                    <div class="c-card__body">


                        <div class="c-card__stat c-card__stat--section">
                            <a class="#" href="{{ url('admin/product/show') }}">
                                <div class="c-card__stat-value c-card__stat-value--active">
                                    <span dir="ltr">{{DB::table('products')->count()}}</span>
                                </div>
                                <p class="c-card__stat-description">محصولات</p>
                            </a>
                        </div>


                        <a class="c-card__stat js-change-selling-chart" href="{{ url('admin/type/show') }}">
                            <div class="c-card__stat-value" data-value="137897000">
                                <span dir="ltr">{{DB::table('big_s_products_type')->count()}}</span>
                            </div>
                            <p class="c-card__stat-description">تنوع ها</p>
                        </a>

                        <a class="c-card__stat js-change-selling-chart"  href="{{ url('admin/type/show') }}">
                            <div class="c-card__stat-value">
                                <span dir="ltr">{{DB::table('big_s_products_type')->where("Avaliable","=","1")->count()}}</span>
                            </div>
                            <p class="c-card__stat-description">تنوع های فعال</p>
                        </a>

                        <a class="c-card__stat js-change-selling-chart"  href="{{ url('admin/type/show') }}">
                            <div class="c-card__stat-value">
                                <span dir="ltr">{{DB::table('big_s_products_type')->where("Avaliable","=","0")->count()}}</span>
                            </div>
                            <p class="c-card__stat-description">تنوع های غیر فعال</p>
                        </a>
                    </div>
                </div>
            </div>

            <div class="c-grid__col c-grid__col--lg-4" style="padding-top: 5%;">
                <div class="c-card">
                    <div class="c-card__header">
                        <h2 class="c-card__title">آمار بازدید (بزودی)</h2>
                    </div>
                    <div class="c-card__body"  style="  -webkit-filter: blur(5px);
                    -moz-filter: blur(5px);
                    -o-filter: blur(5px);
                    -ms-filter: blur(5px);
                    filter: blur(5px);">


                        <div class="c-card__stat-value" data-value="137897000">
                            <span dir="ltr">369</span>
                        </div>
                        <p class="c-card__stat-description">بازدیدهای امروز از سایت</p><br />


                        <div class="c-card__stat-value" data-value="137897000">
                            <span dir="ltr">433</span>
                        </div>
                        <p class="c-card__stat-description">بازدیدهای امروز از سایت</p><br />


                        <div class="c-card__stat-value" data-value="137897000">
                            <span dir="ltr">6</span>
                        </div>
                        <p class="c-card__stat-description">بازدیدهای امروز از سایت</p><br />


                        <div class="c-card__stat-value">
                            <span dir="ltr">9</span>
                        </div>
                        <p class="c-card__stat-description">بازدید های امروز از محصولات</p><br />


                    </div>
                </div>
            </div>


        </div>



    </div>
</main>
@endsection