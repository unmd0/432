<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>داشبورد</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
@yield('meta_add')
    @include('admin.master.layouts.links.index')
    
</head>

<body>
    @include('admin.master.layouts.header.index')
    @include('admin.master.layouts.menu.index')
    @yield('admin-main')
    @include('admin.master.layouts.footer.index')
</body>

</html>