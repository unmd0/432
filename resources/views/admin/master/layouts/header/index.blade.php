<header class="c-header js-header">
    <div class="uk-container uk-container-large">
        <div class="c-header__top">
            <h1 class="c-header__logo"><a href=""></a></h1>
            <a href="/admin" style="text-decoration: none;"><h2 class="c-header__tag">مدیر {{Auth::user()->name}}</h2></a>
        </div>
    </div>
</header>