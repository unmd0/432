<div class="c-header__nav js-header-nav">
    <div class="uk-container uk-container-large uk-container--responsive">



        {{-- This button for responsive MENU --}}
        <button class="uk-navbar__control uk-navbar__control--collapse js-navbar-control" type="button"
            aria-label="show menu">
            <span class="uk-navbar__control-box">
                <span class="uk-navbar__control-inner"></span>
            </span>
        </button>
        {{-- This button for responsive MENU --}}



        <nav class="uk-navbar-container uk-navbar-transparent uk-navbar--responsive uk-navbar" uk-navbar="">
            <div class="uk-navbar-right" id="dashboard-step-1">
                <ul class="uk-navbar-nav uk-navbar-nav--responsive">

                    {{-- including all files in "master/layouts/menu/li" --}}
                        @include('admin.master.layouts.menu.li.1li')
                        @include('admin.master.layouts.menu.li.2li')
                        @include('admin.master.layouts.menu.li.3li')
                    {{-- including all files in "master/layouts/menu/li" --}}

                </ul>
            </div>

            <div class="uk-navbar-left">
                <ul class="uk-navbar-nav uk-navbar-nav--responsive">
                    <li>
                        <a href="{{ url('admin/') }}"> خوش آمدید ...
                            <span class="chevron-down"></span>
                        </a>
                        <div class="uk-navbar-dropdown uk-navbar-dropdown--has-rating">

                            <ul class="uk-nav uk-navbar-dropdown-nav">
                                <li>
                                    <a href="{{ url('admin/') }}" class="has-icon">
                                        <span uk-icon="icon: clock;" class="uk-icon"></span>
                                        داشبورد
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('admin/profile') }}" class="has-icon">
                                        <span uk-icon="icon: user;" class="uk-icon"></span>
                                        پروفایل شما
                                    </a>
                                </li>
                                <li>
                                <form action="{{route('logout')}}" method="POST">
                                    @csrf
                                    <button type="submit">
                                        <span uk-icon="icon: ban;" class="uk-icon"></span>
                                        خروج
                          
                                </button>
                                </form>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>




    </div>
</div>