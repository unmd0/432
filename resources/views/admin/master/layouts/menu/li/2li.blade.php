<li class="first-level">
    <a href="{{ url('admin/orders') }}">سفارشات
        <span class="chevron-down"></span>
    </a>
    <div class="uk-navbar-dropdown">
        <ul class="uk-nav uk-navbar-dropdown-nav">
            <li>
                <a href="{{ url('admin/orders') }}">
                    مدیریت سفارشات
                </a>
            </li>
        </ul>
    </div>
</li>