<li class="first-level">
    <a href="{{ url('admin/product/show') }}">محصولات
        <span class="chevron-down"></span>
    </a>
    <div class="uk-navbar-dropdown">
        <ul class="uk-nav uk-navbar-dropdown-nav">
            <li>
                <a href="{{ url('admin/product/add') }}">
                    درج محصول
                </a>
            </li>
        </ul>
        <ul class="uk-nav uk-navbar-dropdown-nav">
            <li>
                <a href="{{ url('admin/product/show') }}">
                    مدیریت محصولات
                </a>
            </li>
        </ul>
        <ul class="uk-nav uk-navbar-dropdown-nav">
            <li>
                <a href="{{ url('admin/type/show') }}">
                    مدیریت تنوع ها
                </a>
            </li>
        </ul>
    </div>
</li>