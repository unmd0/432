<footer class="c-footer">
    <div class="c-footer__top">
        <div class="uk-container uk-container-large">
            <div class="uk-flex uk-flex-between uk-flex-middle">
                <ul class="uk-subnav uk-subnav-divider">

                    <li>
                        <a
                            href="https://selleracademy.digikala.com/%d9%81%d8%b1%d9%85-%d8%aa%d9%85%d8%a7%d8%b3-%d8%a8%d8%a7-%d9%85%d8%a7/">
                            پشتیبانی
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="c-footer__bottom">
        <div class="uk-container uk-container-large">
            <div class="uk-flex uk-flex-between">
                <div></div>
                <div><a href="">www.bid.com</a> 2020</div>
            </div>
        </div>
    </div>
</footer>
<script src="/js/index.js"></script>
<script src="/js/dropdown.min.js"></script>
<script src="/js/package.js"></script>