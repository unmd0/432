<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->text('id')->unique();
            $table->text("firstname");
            $table->text("lastname");
            $table->text("state");
            $table->text("city");
            $table->text("address");
            $table->text("zipcode");
            $table->text("phone");
            $table->text("email")->nullable();
            $table->text("desc")->nullable();
            $table->text("status");
            $table->text("message")->nullable();
            $table->text("price");
            $table->text("product");
            $table->timestamps();
        });
        // $prefix = DB::getTablePrefix();
        // DB::update("ALTER TABLE ".$prefix."orders AUTO_INCREMENT = 1112;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
