<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBigSProductsTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('big_s_products_type', function (Blueprint $table) {
            $table->id('TypeId');
            $table->text('ProductId');
            $table->text('Name');
            $table->text('Price');
            $table->text('Avaliable');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    //    Schema::dropIfExists('big_s_products_type');
    }
}
