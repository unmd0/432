<?php

use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/addcart{id?}{name?}{qty?}{price?}{_?}', function ($id,$name,$qty,$price) {
//     Cart::add($id, $name, $qty, $price);
//     return Cart::count();
// });


Route::get('/',function(){
    return view('index.index');
});


Route::get('/admin/product/add', function () {
    return view('admin.product.add');
});
Route::get('{subd}.8190.com', function ($x) {
    dd($x);
    return view('admin.product.add');
});


Route::prefix('admin')->middleware('auth')->group( function () {
        Route::get('/', function () {return view('admin.index');})->name('adminpage');
        Route::put('/product/create', 'admin\ProductController@create')->name("p_create");
        Route::get('/type/create/{ID}', 'admin\ProductController@createtype');
        Route::get('/type/show', 'admin\ProductController@showtype');
        Route::get('/type/search{search1?}', 'admin\ProductController@searchtype');
        Route::get('/product/show', 'admin\ProductController@showproduct');
        Route::get('/product/search{search1?}', 'admin\ProductController@searchproduct');
        Route::get('/product/edit/{ID}', 'admin\ProductController@editproduct');
        Route::put('/product/edit', 'admin\ProductController@updateproduct');
        Route::get('/category/{id}', 'admin\ProductController@category');
        Route::get('/orders/', 'admin\ProductController@orders');
        Route::get('/orders/{id}', 'admin\ProductController@ordersdetails');
        Route::get('/orders/ship/{id}', 'admin\ProductController@ordership');
        Route::get('/profile', 'admin\ProductController@profile');
        Route::put('/profile/update', 'admin\ProductController@updateprofile');
        
});














Route::get('/site/{sitename}', 'StoreController@home');
Route::get('/site/{sitename}/details/{id}', 'StoreController@details');
Route::post('/site/{sitename}/details/{id}/addcart', 'StoreController@addcart');
Route::post('/site/{sitename}/cart/updatecart', 'StoreController@updatecart');
Route::post('/site/{sitename}/cart/deletecart', 'StoreController@deletecart');
Route::get('/site/{sitename}/cart', 'StoreController@cart');
Route::get('/site/{sitename}/checkout', 'StoreController@checkout');
Route::put('/site/{sitename}/checkoutstore', 'StoreController@checkoutstore');
Route::post('/site/{sitename}/order/track', 'StoreController@ordertrack');

Auth::routes();
